package org.jhpiego.inout;

public enum Role {
	ADMIN,
	MANAGER,
	USER;
	
	public String getRole() {
		return this.name();
	}
}
