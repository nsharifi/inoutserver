package org.jhpiego.inout.da;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jhpiego.inout.Role;
import org.jhpiego.inout.Status;
import org.json.JSONObject;


@Entity
@Table(name="user")
public class User {
	@Id @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="position")
	private String position;
	
	@Column(name="badge")
	private String badge;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="department")
	private String department;
	
	@Column(name="pin")
	private String pin;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private Status status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="role")
	private Role role;
	
//	@OneToMany(fetch=FetchType.LAZY, mappedBy="user")
//	private Set<InoutRecord> inoutRecords;
	
	public User(String name, String position, String badge, String email, String phone, String department,
			String pin, Status status, Role role) {
		this.name = name;
		this.position = position;
		this.badge = badge;
		this.email = email;
		this.phone = phone;
		this.department = department;
		this.pin = pin;
		this.status = status;
		this.role = role;
	}
	public User(int id, String name, String position, String badge, String email, String phone, String department,
			String pin, Status status, Role role) {
		this.id = id;
		this.name = name;
		this.position = position;
		this.badge = badge;
		this.email = email;
		this.phone = phone;
		this.department = department;
		this.pin = pin;
		this.status = status;
		this.role = role;
	}
	public User() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	// Testing
	public static void main(String[] args) {
		User user = new User();
		user.setName("Naser Sharifi");
		user.setPin("0000");
		user.setStatus(Status.OUT);
		user.setRole(Role.USER);
		System.out.println(new JSONObject(user));
	}
	
}
