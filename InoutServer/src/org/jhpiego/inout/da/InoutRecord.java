package org.jhpiego.inout.da;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jhpiego.inout.Status;

@Entity
@Table(name="inout_record")
public class InoutRecord {
	@Id @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="timestamp")
	private Date timestamp;
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	private Status status;

	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="user_id")
	private User user;
	
	public InoutRecord(Date timestamp, Status status, User user) {
		this.timestamp = timestamp;
		this.status = status;
		this.user = user;
	}
	public InoutRecord(){}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
