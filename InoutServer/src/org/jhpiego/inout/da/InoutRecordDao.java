package org.jhpiego.inout.da;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

public class InoutRecordDao {
	
	public List<InoutRecord> getInoutRecords() {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		List<InoutRecord> inoutRecords = session.createCriteria(
				InoutRecord.class).list();
		tx.commit();
		session.close();
		return inoutRecords;
	}
	
	// FIXME change this to return the latest entry; now returns the oldest
	public List<InoutRecord> getCurrentStatus() {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		List<InoutRecord> inoutRecords = new ArrayList<InoutRecord>();
		inoutRecords = session.createQuery("from InoutRecord f1 where (f1.timestamp, user) " +
				"in (select max(f.timestamp), f.user from InoutRecord f group by f.user)").list();
		tx.commit();
		session.close();
		return inoutRecords;
	}
	
	public InoutRecord getInoutRecord(int inoutRecordId) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		InoutRecord inoutRecord = session.get(InoutRecord.class, inoutRecordId);
		tx.commit();
		session.close();
		return inoutRecord;
	}
	
	public List<InoutRecord> getPeriodic(String dateFrom, String dateTo) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(InoutRecord.class);
		criteria.add(Restrictions.sqlRestriction("timestamp between '" + 
					dateFrom + "' AND " + "'" + dateTo + "'"));
		List<InoutRecord> inouts = criteria.list();
		tx.commit();
		session.close();
		return inouts;
	}
	
	public List<InoutRecord> getPeriodic(int userId, String dateFrom, String dateTo) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(InoutRecord.class);
		criteria.add(Restrictions.sqlRestriction("user_id=" + userId + " and " +
				"timestamp between '" + dateFrom + "' AND " + "'" + dateTo + "'"));
		List<InoutRecord> inouts = criteria.list();
		tx.commit();
		session.close();
		return inouts;
	}
	
	public List<InoutRecord> getMonthly(int year, int month) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(InoutRecord.class);
		String monthString = month < 10 ? "0" + month : String.valueOf(month); 
		String timestamp = year + "-" + monthString;
//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.YEAR, year);
//		cal.set(Calendar.MONTH, month);
//		Date date = cal.getTime();
		//System.out.println("date: " + date);
		
		criteria.add(Restrictions.sqlRestriction("timestamp like '" + timestamp + "%'"));
//		query.setDate("timestamp", date);
		List<InoutRecord> inouts = criteria.list();
		tx.commit();
		session.close();
		return inouts;
	}
	
	public List<InoutRecord> getUserRecords(int userId, String timestamp) {
		Session session = SessionUtil.getSession();
		String queryString = "from InoutRecord where user.id = :userId and timestamp like :timestamp";
		Query<InoutRecord> query = session.createQuery(queryString);
		query.setInteger("userId", userId);
		query.setString("timestamp", timestamp+"%");
		List<InoutRecord> inouts = query.list();
		
		return inouts;
	}

	public List<InoutRecord> getUserRecords(int userId, String dateFrom, String dateTo) {
		Session session = SessionUtil.getSession();
		String queryString = "from InoutRecord where user.id = :userId and " +
				"timestamp between :dateFrom and :dateTo";
		Query<InoutRecord> query = session.createQuery(queryString);
		query.setInteger("userId", userId);
		query.setString("dateFrom", dateFrom);
		query.setString("dateTo", dateTo);
		List<InoutRecord> inouts = query.list();
		
		return inouts;
	}
	
	public List<InoutRecord> getDaily(int year, int month, int day) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(InoutRecord.class);
		String dayString = day < 10 ? "0"+day : ""+day;
		String monthString = month < 10 ? "0"+month : ""+month;
		String timestamp = year + "-" + monthString + "-" + dayString;
//		Calendar cal = Calendar.getInstance();
//		cal.set(Calendar.YEAR, year);
//		cal.set(Calendar.MONTH, month);
//		Date date = cal.getTime();
		//System.out.println("date: " + date);
		
		criteria.add(Restrictions.sqlRestriction("timestamp like '" + timestamp + "%'"));
		
//		query.setDate("timestamp", date);
		List<InoutRecord> inouts = criteria.list();
		tx.commit();
		session.close();
		return inouts;
	}

	public void addInoutRecord(InoutRecord bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		InoutRecord record = new InoutRecord();
		record.setTimestamp(bean.getTimestamp());
		record.setStatus(bean.getStatus());
		record.setUser(bean.getUser());
		session.save(record);
		tx.commit();
		session.close();
	}
	
	public static void main(String[] args) {
		InoutRecordDao dao = new InoutRecordDao();
		dao.getCurrentStatus();
//		List<InoutRecord> inouts = dao.getMonthly(2016, 10);
//		System.out.println(inouts);
//		UserDao userDao = new UserDao();
//		User user = userDao.getUser(5);
//		InoutRecord inout = inoutDao.getInoutRecord(46);
//		System.out.println(inout.getAssets());
//		inout.setTimestamp(new Date(System.currentTimeMillis()));
//		inout.setUser(user);
//		inoutDao.addInoutRecord(inout);
	}
}
