package org.jhpiego.inout.da;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.jhpiego.inout.Status;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserDao {
	
	public void addUser(User bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		User user = bean;
		session.save(user);
		tx.commit();
		session.close();
	}
	
	public List<User> getUsers() {
		Session session = SessionUtil.getSession();
		Query<User> query = session.createQuery("from User where status != :inactive order by name");
		query.setString("inactive", "INACTIVE");
		List<User> users = query.list();
		session.close();
		return users;
	}
	
	public User getUser(int id) {
		Session session = SessionUtil.getSession();
		User user = session.get(User.class, id);
		session.close();
		return user;
	}
	
	public void updateUser(User bean) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		User user = session.get(User.class, bean.getId());
		user.setBadge(bean.getBadge());
		user.setDepartment(bean.getDepartment());
		user.setEmail(bean.getEmail());
		user.setName(bean.getName());
		user.setPhone(bean.getPhone());
		user.setPin(bean.getPin());
		user.setPosition(bean.getPosition());
		user.setStatus(bean.getStatus());
		user.setRole(bean.getRole());
		session.update(user);
		tx.commit();
		session.close();
	}
	
	public void changeStatus(int userId) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		User user = session.get(User.class, userId);
//		System.out.println("user: " + user);
		if (user.getStatus() == Status.IN) {
			user.setStatus(Status.OUT);
		} else {
			user.setStatus(Status.IN);
		}
		session.persist(user);
		tx.commit();
		session.close();
	}
	
	public void deactivate(int userId) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		User user = session.get(User.class, userId);
		user.setStatus(Status.INACTIVE);
		session.persist(user);
		tx.commit();
		session.close();
	}
	
	public User authenticate(String email, String pin) {
		Session session = SessionUtil.getSession();
		Transaction tx = session.beginTransaction();
		Query<User> query = session.createQuery("from User where email = :email and pin = :pin");
		query.setString("email", email);
		query.setString("pin", pin);
		User user = query.getResultList().get(0);
		session.close();
		return user;
	}
		
	// Test it
	public static void main(String[] args) {
		UserDao dao = new UserDao();
//		System.out.println(new JSONObject(dao.getUser(74)));
//		dao.changeStatus(74);
		//JSONArray jsonUsers = new JSONArray(dao.getUsers());
		//System.out.println(jsonUsers.get(74));
		//		List<User> users = dao.getUsers();
		//dao.deactivate(123);
		//System.out.println(dao.authenticate("Naser.Sharifi@jhpiego.org", "0000"));
	}
}
