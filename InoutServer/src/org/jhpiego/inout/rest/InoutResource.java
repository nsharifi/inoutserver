package org.jhpiego.inout.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jhpiego.inout.Status;
import org.jhpiego.inout.da.InoutRecord;
import org.jhpiego.inout.da.InoutRecordDao;
import org.jhpiego.inout.da.User;
import org.jhpiego.inout.da.UserDao;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("/inoutrecords")
public class InoutResource {
	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public String getInoutRecords() {
//		InoutRecordDao dao = new InoutRecordDao();
//		List<InoutRecord> inoutRecords = dao.getInoutRecords();
//		JSONArray jsonInoutRecords = new JSONArray();
//		for (int i = 0; i < inoutRecords.size(); i++) {
//			JSONObject jsonInoutRecord = new JSONObject(inoutRecords.get(i));
//			jsonInoutRecords.put(jsonInoutRecord);
//		}
//		return jsonInoutRecords.toString();
//	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/current")
	public String getCurrentStatus() {
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inoutRecords = dao.getCurrentStatus();
		JSONArray jsonInouts = new JSONArray();
		JSONObject jsonInout = null;
		for (InoutRecord inout : inoutRecords) {
			jsonInout = new JSONObject();
			jsonInout.put("id", inout.getId());
			jsonInout.put("timestamp", inout.getTimestamp());
			jsonInout.put("status", inout.getStatus());
			jsonInout.put("user", inout.getUser().getName());
			jsonInouts.put(jsonInout);
		}
		return jsonInouts.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/single/{user_id}/{date_from}/{date_to}")
	public String getSingleUser(@PathParam("user_id") int userId,
			@PathParam("date_from") String dateFrom,
			@PathParam("date_to") String dateTo) {

		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inoutRecords = dao.getUserRecords(userId, dateFrom, dateTo);
		JSONArray jsonInouts = new JSONArray();
		JSONObject jsonInout = null;
		for (InoutRecord inout : inoutRecords) {
			jsonInout = new JSONObject();
			jsonInout.put("id", inout.getId());
			jsonInout.put("timestamp", inout.getTimestamp());
			jsonInout.put("status", inout.getStatus());
			jsonInout.put("user", inout.getUser().getName());
			jsonInouts.put(jsonInout);
		}
		return jsonInouts.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/single/{user_id}/{timestamp}")
	public String getSingleUser(@PathParam("user_id") int userId,
			@PathParam("timestamp") String timestamp) {
//		System.out.println("timestamp: " + timestamp);
//		System.out.println("userId: " + userId);
System.out.println("userId: " + userId + " timestamp: " + timestamp);
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inoutRecords = dao.getUserRecords(userId, timestamp);
		JSONArray jsonInouts = new JSONArray();
		JSONObject jsonInout = null;
		for (InoutRecord inout : inoutRecords) {
			jsonInout = new JSONObject();
			jsonInout.put("id", inout.getId());
			jsonInout.put("timestamp", inout.getTimestamp());
			jsonInout.put("status", inout.getStatus());
			jsonInout.put("user", inout.getUser().getName());
			jsonInouts.put(jsonInout);
		}
		return jsonInouts.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/periodictime/{date_from}/{date_to}")
	public String getPeriodicList(@PathParam("date_from") String dateFrom,
			@PathParam("date_to") String dateTo) {
		JSONArray jsonResult = new JSONArray();
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inouts = dao.getPeriodic(dateFrom, dateTo);
		JSONObject jsonRecord = null;
		Map<User, Long> userHours = new HashMap<User, Long>();
		//Map<User, Boolean> userIsIn = new HashMap<User, Boolean>();
		Map<User, Date> userPendingTime = new HashMap<User, Date>();
		
		for (InoutRecord inout : inouts) {
			User u = inout.getUser();
			if (userHours.containsKey(u)) {
				if (inout.getStatus() == Status.OUT) {
					userHours.put(u, userHours.get(u) + 
							(inout.getTimestamp().getTime() - userPendingTime.get(u).getTime()));
				} else if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			} else {
				userHours.put(u, 0L);
				if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			}
		}
		
		for (User user : userHours.keySet()) {
			jsonRecord = new JSONObject();
			jsonRecord.put("userId", user.getId());
			jsonRecord.put("user", user.getName());
			double hrsCount = (double)userHours.get(user) / (1000 * 60 * 60);
			jsonRecord.put("hours", hrsCount);
			jsonResult.put(jsonRecord);
		}
		return jsonResult.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/periodictime/{user_id}/{date_from}/{date_to}")
	public String getPeriodicList(@PathParam("user_id") int userId,
			@PathParam("date_from") String dateFrom,
			@PathParam("date_to") String dateTo) {
		JSONArray jsonResult = new JSONArray();
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inouts = dao.getPeriodic(userId, dateFrom, dateTo);
		JSONObject jsonRecord = null;
		Map<User, Long> userHours = new HashMap<User, Long>();
		//Map<User, Boolean> userIsIn = new HashMap<User, Boolean>();
		Map<User, Date> userPendingTime = new HashMap<User, Date>();
		
		for (InoutRecord inout : inouts) {
			User u = inout.getUser();
			if (userHours.containsKey(u)) {
				if (inout.getStatus() == Status.OUT) {
					userHours.put(u, userHours.get(u) + 
							(inout.getTimestamp().getTime() - userPendingTime.get(u).getTime()));
				} else if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			} else {
				userHours.put(u, 0L);
				if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			}
		}
		
		for (User user : userHours.keySet()) {
			jsonRecord = new JSONObject();
			jsonRecord.put("userId", user.getId());
			jsonRecord.put("user", user.getName());
			double hrsCount = (double)userHours.get(user) / (1000 * 60 * 60);
			jsonRecord.put("hours", hrsCount);
			jsonResult.put(jsonRecord);
		}
		return jsonResult.toString();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/monthlytime/{year}/{month}")
	public String getMonthlyList(@PathParam("year") int year,
			@PathParam("month") int month) {
//		System.err.println("Printing monthly time now...");
		JSONArray jsonResult = new JSONArray();
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inouts = dao.getMonthly(year, month);
		JSONObject jsonRecord = null;
		Map<User, Long> userHours = new HashMap<User, Long>();
		//Map<User, Boolean> userIsIn = new HashMap<User, Boolean>();
		Map<User, Date> userPendingTime = new HashMap<User, Date>();
//		System.out.println("number of inouts: " + inouts.size());
		for (InoutRecord inout : inouts) {
			User u = inout.getUser();
			if (userHours.containsKey(u)) {
				if (inout.getStatus() == Status.OUT) {
					userHours.put(u, userHours.get(u) + 
							(inout.getTimestamp().getTime() - userPendingTime.get(u).getTime()));
				} else if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			} else {
				userHours.put(u, 0L);
				if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			}
		}
		
		for (User user : userHours.keySet()) {
			jsonRecord = new JSONObject();
			jsonRecord.put("userId", user.getId());
			jsonRecord.put("user", user.getName());
			double hrsCount = (double)userHours.get(user) / (1000 * 60 * 60);
			jsonRecord.put("hours", hrsCount);
			jsonResult.put(jsonRecord);
		}
		return jsonResult.toString();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/dailytime/{year}/{month}/{day}")
	public String getDailyList(@PathParam("year") int year,
			@PathParam("month") int month,
			@PathParam("day") int day) {
		JSONArray jsonResult = new JSONArray();
		InoutRecordDao dao = new InoutRecordDao();
		List<InoutRecord> inouts = dao.getDaily(year, month, day);
		JSONObject jsonRecord = null;
		Map<User, Long> userHours = new HashMap<User, Long>();
		//Map<User, Boolean> userIsIn = new HashMap<User, Boolean>();
		Map<User, Date> userPendingTime = new HashMap<User, Date>();
		
		for (InoutRecord inout : inouts) {
			User u = inout.getUser();
			if (userHours.containsKey(u)) {
				if (inout.getStatus() == Status.OUT) {
					userHours.put(u, userHours.get(u) + 
							(inout.getTimestamp().getTime() - userPendingTime.get(u).getTime()));
				} else if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			} else {
				userHours.put(u, 0L);
				if (inout.getStatus() == Status.IN) {
					userPendingTime.put(u, inout.getTimestamp());
				}
			}
		}
		
		for (User user : userHours.keySet()) {
			jsonRecord = new JSONObject();
			jsonRecord.put("userId", user.getId());
			jsonRecord.put("user", user.getName());
			double hrsCount = (double)userHours.get(user) / (1000 * 60 * 60);
			jsonRecord.put("hours", hrsCount);
			jsonResult.put(jsonRecord);
		}
		return jsonResult.toString();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// FIXME return failure (by setting status code and returned JSON) if operation fails
	public String createInoutRecord(
			@FormParam("user_id") int userId) {
		
//		int userId = jsonData.getInt("user_id");
//		int destId = jsonData.getInt("dest_id");
//		JSONArray assetIds = jsonData.getJSONArray("asset_ids");
//System.out.println("user ID: " + userId);
		UserDao userDao = new UserDao();
//		User user = userDao.getUser(userId);
		userDao.changeStatus(userId);
		User user = userDao.getUser(userId);
		
		// TODO Inspect cardinality of destination
		InoutRecordDao inoutDao = new InoutRecordDao();
		InoutRecord inoutRecord = new InoutRecord();
		inoutRecord.setStatus(user.getStatus());
		inoutRecord.setTimestamp(new Date(System.currentTimeMillis()));
		inoutRecord.setUser(user);
		inoutDao.addInoutRecord(inoutRecord);
		
		return "{'result': 'success'}";
	}
	
	public static void main(String[] args) {
		InoutResource ir = new InoutResource();
		System.out.println(ir.getMonthlyList(2016, 10));
	}
}
