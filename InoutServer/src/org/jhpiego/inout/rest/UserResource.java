package org.jhpiego.inout.rest;

import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jhpiego.inout.Role;
import org.jhpiego.inout.Status;
import org.jhpiego.inout.da.User;
import org.jhpiego.inout.da.UserDao;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("/users")
public class UserResource {
//	@GET
//	@Produces("application/json")
//	public String printHello(){
//		return "{output: Hello world!}";
//	}
	
	@GET
	@Produces("application/json")
	public String getUsers() {
		UserDao dao = new UserDao();
		List<User> users = dao.getUsers();
		JSONArray jsonUsers = new JSONArray();
		JSONObject jsonUser;
		for (User user : users) {
			jsonUser = new JSONObject(user);
			jsonUsers.put(jsonUser);
		}
		
		return jsonUsers.toString();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(@PathParam("id") int id) {
		UserDao dao = new UserDao();
		User user = dao.getUser(id);
		JSONObject jsonUser = new JSONObject(user);
		
		return jsonUser.toString();
	}
	
	@PUT
	@Path("/changestatus")
	@Produces(MediaType.TEXT_PLAIN)
	public String changeStatus(@FormParam("id") int id) {
		UserDao dao = new UserDao();
		dao.changeStatus(id);
		return "success";
	}
	
	@PUT
	@Path("/deactivate/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String deactivate(@PathParam("id") int id) {
		UserDao dao = new UserDao();
		dao.deactivate(id);
		return "{result: success}";
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public String login(@FormParam("email") String email,
			@FormParam("pin") String pin) {
		//System.out.println("Email: " + email);
		UserDao dao = new UserDao();
		dao.authenticate(email, pin);
		JSONObject jsonUser = new JSONObject(dao.authenticate(email, pin));
		return jsonUser.toString();
	}
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateUser(
			@PathParam("id") int id,
			@FormParam("name") String name,
			@FormParam("position") String position,
			@FormParam("badge") String badge,
			@FormParam("phone") String phone,
			@FormParam("email") String email,
			@FormParam("department") String department,
			@FormParam("pin") String pin,
			@FormParam("status") Status status,
			@FormParam("role") Role role) {
		
		UserDao dao = new UserDao();
		User user = 
				new User(id, name, position, badge, email, phone, department, pin, status, role);
		dao.updateUser(user);
		
		return "{result: success}";
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String createUser(
			@FormParam("name") String name,
			@FormParam("position") String position,
			@FormParam("badge") String badge,
			@FormParam("email") String email,
			@FormParam("phone") String phone,
			@FormParam("department") String department,
			@FormParam("pin") String pin,
			@FormParam("status") Status status,
			@FormParam("role") Role role) {
		
		User user = new User(name, position, badge, email, phone, department, pin, status, role);
		UserDao dao = new UserDao();
		dao.addUser(user);
		
		return "{result: success}";
	}
	
	// Testing
	public static  void main(String[] args) {
		UserResource r = new UserResource();
		
		
		//String result = r.getUsers();
		//System.out.println(result);
	}
}
