package org.jhpiego.inout;

public enum Status {
	IN, OUT, INACTIVE;
	
	public String getStatus() {
		return this.name();
	}
}
