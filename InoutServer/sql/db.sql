CREATE DATABASE `inout`;
USE `inout`;

CREATE TABLE `user` (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(150) NOT NULL,
	position VARCHAR(50),
	badge VARCHAR(50),
	email VARCHAR(100) NOT NULL,
	phone VARCHAR(20),
	department VARCHAR(100),
	pin CHAR(4) NOT NULL DEFAULT '0000',
	status ENUM('IN', 'OUT', 'INACTIVE') NOT NULL DEFAULT 'OUT',
	role ENUM('USER', 'MANAGER', 'ADMIN') NOT NULL DEFAULT 'USER',
	PRIMARY KEY(id),
	UNIQUE (email)
);

CREATE TABLE inout_record (
	id INT NOT NULL AUTO_INCREMENT,
	timestamp DATETIME,
	user_id INT,
	status ENUM('IN', 'OUT'),
	PRIMARY KEY(id),
	FOREIGN KEY(user_id) REFERENCES `user`(id)
);


insert into `user`(name, position, phone, email, pin) VALUES('Nasratullah Ansari','Country Director / Chief of Party','0093(0)796171719','Nasratullah.Ansari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Faridullah Atiqzai','Technical Director / Deputy Chief of Party','0093(0)700209588','Faridullah.Atiqzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Rashed Wassif','Innovation Fund Team Leader','0093(0)799201038','Rashed.Wassif@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Humayoon Gardiwal','Innovation Fund Advisor','0093(0)700604505','Humayoon.gardiwal@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Shafiq Ahmad Yousofi','Innovation Fund Coordinator ','0093(0)774544304','Shafiq.Yousofi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Partamin','Monitoring, Evaluation and Research  Director ','0093(0)700020686','Partamin@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Eklil Hossain','Monitoring, Evaluation and Research Manager','0093(0)799212860','Eklil.Hossain@jhpiego.org ','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mahmood Azimi','MIS / GIS Advisor','0093(0)799278687','Mahmood.Azimi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Benafsha Noory','Monitoring, Evaluation and Research Advisor ','0093(0)799303808','Benafsha.Noory@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Said Raouf Saidzada','Knowledge Management Advisor','0093(0)799266013','Raouf.Saidzada@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Ahmad Esmati','Quality of Care Study Advisor','0093(0)700358500','Sayed.Esmati@Jhpiego.org) ','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Qader Rahimi','MIS / GIS Senior Officer ','0093(0)700289060','Qader.Rahimi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Matiullah Noorzad','Senior Monitoring, Evaluation and Research Officer ','0093(0)799550073','Matiullah.Noorzad@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zahra Sultani','Monitoring, Evaluation and Research Officer','0093(0)766232636','Zahra.Sultani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Laila Natiq','Quality Improvement Team Leader','0093(0)780016703','Laila.Natiq@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Feroza Mushtari','Midwifery & Nursing Manager ','0093(0)778823927','Feroza.Mushtari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Basir Farid','Quality Improvement Manager','0093(0)700707711','Basir.Farid@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Farzana Maruf','Maternal and Newborn Health Officer ','0093(0)798408011','Farzana.Maruf@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Najibullah Naimi','Professional Association Straightening Advisor','0093(0)799010838 ','Najibullah.Naimi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Habiburahman Hussainy','Quality Improvement Advisor  ','0093(0)772765956','habiburahman18@gmail.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Minqad u Rahman Roadwal','Quality Improvement Advisor  ','0093(0)700299616','Minqad.Roadwal@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Salem Asgherkhil','Quality Improvement officer','0093(0)799874494','asgherkhilwardak@gmail.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Nazifa Rahim','Quality Improvement officer','0093(0)780511658','nazifaim@yahoo.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Karima Mayar Amiri','Quality Improvement specialist','0093(0)792230018','karima.mayar@yahoo.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Najiba Yaftali','Quality Improvement specialist','0093(0)799081618','zianajiba02@gmial.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Mohammad Reza Safdari','Nursing Advisor','0093(0)799593006','SayedReza.Safdari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Massoma Jafari','Midwifery Advisor ','0093(0)796858875','Massoma.Jafari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zohra Sadat Hashemi','Senior Nursing Officer ','0093(0)796689529','Zohra.Sadat@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Kobra Haidari','Midwifery Officer','0093(0)799531080','Kobra.Haidari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Jamila Amin','Provincial Midwifery Officer (Paktiya)','0093(0)770687804','Jamila.Amin@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Samia Ehsas','Provincial Midwifery Officer (Ghazni)','0093(0)790641647','Samia.Ehsas@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Nooria Qaderi','Provincial Midwifery Officer (Baghlan)','0093(0)798009440','Drnooria.qaderi@gmail.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Masood Arzoiy','Service Delivery Team Leader','0093(0)799382006','Masood.Arzoiy@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Kamran Osmani','Family Planning Advisor ','0093(0)799898383','Kamran.Osmani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Niaz Mohammad Popal','Newborn & Child Health Advisor ','0093(0) 799189487 ','Niaz.Popal@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Nadim Kaihan Niazi','PPH Prevention Advisor','0093(0)777292293','Nadim.Niazi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Rahila Joya','Gender Advisor','0093(0)799489489','Rahila.Joya@thepalladiumgroup.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Shakoor Hatifie','Youth and Adolescent Advisor                  ','0093(0)799457989','Shakoor.Hatifie@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Masood Rahmani','Private Sector Advisor','0093(0)703334334','Masoud.Rahmani@thepalladiumgroup.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Malalai Jamshid Nejaby','Maternal Health Advisor','0093(0)700258961','malalainejaby@gmail.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Naseer Ahmad Durrani','Family Planning integration Advisor ','0093(0)775535151','naserdurani@yahoo.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ghutai Sadeq Yaqubi','Senior Family Planning  Officer','0093(0)799349219','Ghutai.Sadeq@Jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Adela Kohestani','Maternal Newborn and Child Health Officer ','0093(0)798110298','Adela.Kohistani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Nilofar Sultani','Gender Coordinator','0093(0)700067609','nilofar.sultani@thepalladiumgroup.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zabihullah Rahmani','Field Operations Team Leader','0093(0)786526707','Zabihullah.rahmani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Khesraw.parwiz','Field Operations Advisor','0093(0)799566644','Khesraw.Parwiz@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Ahmad Gawhari','Regional Program Manager, Mazar','0093(0)700502771','Sayedahmad.Gawhari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Khadija Arian changezi','Provincial Coordinator, Badakhshan','0093(0)700242000','Khadija.Arian@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sakhi Sardar','Provincial Coordinator, Khost','0093(0)708889091','Sakhi.Sardar@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Eid Mohammad Tariq','Provincial Coordinator, Ghor','0093(0)798233040','Eid.Tariq@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Noor Hassan Shirzad','Provincial Coordinator, Helmand','0093(0)703433646','Noorhassan.Shirzad@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Muhammad Shah Katawazai','Provincial Coordinator, Pakteka','0093(0)798298060','Muhammad.Katawazai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Yama Wardoji','Provincial Coordinator, Takhar','0093(0)797075190','Yama.Wardoji@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Bezhan Naseri','Provincial Coordinator, Balkh','0093(0)799158258','Bezhan.Naseri@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zakiullah Storay','Provincial Coordinator, kunar','0093(0)775483774','Zakiullah.Storay@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zulfiqar Ashrafi','Provincial Coordinator, Baghlan','0093(0)799385157','Zulfiqar.Ashrafi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sultan Mahmood Bazel','Provincial Coordinator, Faryab','0093(0)798293932','Sultanmahmood.Bazel@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Zubair Khadim','Provincial Coordinator, Laghman','0093(0) 797075773','Zubair.khadim@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Bashir Ahmad Omid','Provincial Coordinator, Ghazni','0093(0)797742129','Bashir.Ahmad@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Shiraga Mohmand','Provincial Coordinator, Paktya','0093(0)781381588','Sheragha.mohmmand@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ezatullah Azimi','Provincial Coordinator, Panjshir','0093(0)774638476','Ezatullah.Azimi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Namir Safi','Provincial Coordinator, Jawzjan','0093(0)700511123','Namir.Safi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Shah Mohammad Qazizada','Provincial Coordinator, Nangarhar','0093(0)798241630','Shahmohammad.Qazizada@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Mohammad Saeed Hamedi','Provincial Coordinator, Herat','0093(0)799205606','Sayed.Hamedi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Rustam Royeen','Provincial Coordinator, Nimroz','0093(0)798443198','Rustam.royeen@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Aminullah Mahboobi','Provincial Coordinator, Kandahar','0093(0)799813830','Aminulla.Mahboobi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Yousuf Haqbeen','Provincial Coordinator, Daikundi','0093(0)775370719','Yousuf.Haqbeen@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Sadiq Sarshar','Provincial Coordinator, Badghis','0093(0)799779764','Sadiq.Sarshar@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Haroon Rastagar','Demand Generation Team Leader','0093(0)700440556','Srastagar@fhi360.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Waheed Adeeb','Health Communication and Liaison Coordinator','0093(0)799412680','awadeeb@fhi360.org ','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sayed Omar Alami','ICT Demand Generation Technical Specialist','0093(0)793 332333','Salami@fhi360.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Qiamuddin Samady','Social and Behavior Change Communication Officer','0093(0)747674030','qsamady@fhi360.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Shah Saltani','Demand Generation ICT officer ','0093(0)798321201','asultani@fhi360.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Naser Sharifi','ICT4D Manager','0093(0)789204172','Naser.Sharifi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Alireza Rezaee','ICT4D Officer','0093(0)799600704','Ali.Rezaee@Jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hassan Zakizadeh','Creative  Specialist','0093(0)777146666','Hassan.Zakizadeh@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zarina Faizi','Graphic Designer','0093(0)778211953','Zarina.Faizi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Amina Ahmadi','Intern','0093 79 721 7667','Amina19lashkari@gmail.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Masooma Faizi','Human Resources Officer','0093(0)777273761','Masooma.Faizi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Nilofar Sidiqi','Human Resources Assistant ','0093(0)782578752','Nilofar.Sidiqi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Rayesa','Baby Sitter','0093(0)765884829','Rayesa','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Wahida Dastagirzada','Finance Manager','0093(0)791400308','Wahida.Dastageer@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Fawad Ahmadzai','Finance Officer','0093(0)744464686','Fawad.Ahmadzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Kamaluddin Alokozay','Finance Officer','0093(0)778476490','Kamaluddin.Alokozay@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Dawood Taiman','Accountant Officer','0093(0)799722154','Dawood.taiman@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sadaf Qambari','Cashier','0093(0)795559821','Sadaf.Qambari@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad yusuf Tarin','Finance Assistant','0093(0)793110004','yusuf.Tarin@yahoo.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ali Hammad Rajpoot','Director of Finance and Admin','0093(0)794314892 ','Ali.Rajpoot@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Pamir Usmani','Administration Coordinator ','0093(0)780818010','Pamir.Usmani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Najwa Stanekzai','Administation Officer','0093(0)780112600','Najwa.Stanekzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Maryam Roeen','Office Assistant ','0093(0)729127018','Maryam.Roeen@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('fahad Amiri','Admin Intern','0093(0)789418774','fahad.amiri2010@yahoo.com','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Karima Gulabzai','Receptionist ','0093(0)792420230','Karima.Gulabzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Atta Mohammad Mobin','Logistic Officer ','0093(0)789733100','Attamohammad.Mobin@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Khayber Seddiqi','procurment coordinator ','0093(0)798969296','Khyber.Seddiqi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Hadi Rasheedi','Procurement Officer',' 0093(0)799807826','Abdul.Hadi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Fazalhaq Saberi','Procurement Officer','0093(0)794499799','Fazalhaq.Saberi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Atiqullah Salarzai','Procurement Assistant ','0093(0)796884299','Atiqullah.Salarzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Farhad Sultani','Logistic Assistant ','0093(0)799312367','Farhad.Sultani@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Farida Esmat','Inventory Officer','0093 (0)797141932','Farida.Esmat@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hassan Ali Allahyar','Workshop Officer','0093(0)799607486','Hassan.Allahyar@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ruohullah Stanekzai','Workshop Assistant','0093(0)786633316','Rohullah.stanikzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahamd Shekib Rasuli','Maintenance Assistant ','0093(0)799304999','AhamdShekibRasuli','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Parwana','Cleaner','0093(0)796630520','Parwana','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Freba Habibi','Cleaner','0093(0)790161325','FrebaHabibi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Mussa','Cleaner','0093(0)773855271','MohammadMussa','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mir Qayoumuddin','Cleaner','0093(0)784489620','MirQayoumuddin','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Karim','Cleaner','0093(0)792747166','MohammadKarim','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Wazir Mohammad','Cook Assistant','0093(0)781580487','WazirMohammad','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hesanullah','Cook Assistant','0093(0)799390652','Hesanullah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Nasima','Cook Assistant','0093(0)793260075','Nasima','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Haris Nashir','Grants Manager','0093(0)707610300','Haris.Nashir@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Khan Wali Daudzai','Senior Grants Officer','0093(0)796909000','KhanWali.Daudzai@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Monir Ahmad Alam','IT Officer','0093(0)700581919','Monir.Alam@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sohrab Raufi','IT Assistant ','0093(0)790251515','Sohrab.Raufi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hukum Khan Rasolly','Safety and Security Director ','0093(0)787601113','HukumKhan.Rasolly@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Obaidullah Wahedi','Security Coordinator','0093(0)792898992','Obaidullah.Wahedi@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Amiri','Travel Officer','0093(0)788435041','Ahmad.Amiri@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hameedullah Khesraw','Despatcher','0093(0)700211726','Hameedullah.Khesraw@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Salem','Despatcher','0093(0)799501370','MohammadSalem','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ghulam Hazrat','Guard Supervisor','0093(0)700187314','GhulamHazrat','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Wazir Agha','Security Guard ','0093(0)780690959','WazirAgha','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Latif','Security Guard ','0093(0)772191460','AbdulLatif','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Farooq','Security Guard ','0093(0)792898978','MohammadFarooq','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Zabi','Security Guard ','0093(0)788486634','MohammadZabi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Shamsulhaq','Security Guard ','0093(0)772476003','Shamsulhaq','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Sami','Security Guard ','0093(0)700196671','AbdulSami','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mukhtar Ahmad','Security Guard ','0093(0)700218707','MukhtarAhmad','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Hakim','Security Guard ','0093(0)787640634','AbdulHakim','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Feda Mohammad','Security Guard ','0093(0)700028294','FedaMohammad','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Haroon','Security Guard ','0093(0)781542884','Haroon','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Jamil','Security Guard ','0093(0)798799483','AbdulJamil','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Tamim','Security Guard ','0093(0)771211415','MohammadTamim','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Khalilullah Mohammadi','Security Guard ','0093(0)700386730','KhalilullahMohammadi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ahmad Shafiq','Security Guard ','0093(0)794111969','AhmadShafiq','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hamidullah','Security Guard ','0093(0)700182749','Hamidullah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hashmatullah Niazi','Security Guard ','0093(0)772444644','HashmatullahNiazi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Najibullah','Security Guard ','0093(0)700019128','Najibullah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Habib','Security Guard ','0093(0)773341711','AbdulHabib','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Parwiz','Security Guard ','0093(0)786424958','Parwiz','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Ghulam Sanaye','Security Guard ','00930)0778134564','GhulamSanaye','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hayat Khan','Security Guard ','00930)0703460458','HayatKhan','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Najibullah Faizi','Driver ','0093(0)790793117','NajibullahFaizi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Mohammad Zaman','Driver ','0093(0)700605593','MohammadZaman','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Qurban Ali Rahimi','Driver ','0093(0)794501656','QurbanAliRahimi','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Hurmatullah','Driver','0093(0)799022171','Hurmatullah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Samim Shah','Driver','0093(0)702029408','SamimShah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Bashir Ahmad','Driver','0093(0)700248272','BashirAhmad','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Abdul Saboor','Driver','0093(0)777929192','AbdulSaboor','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Zekrullah','Driver','0093(0)783280324','Zekrullah','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Faiz Mohammad','Driver','0093(0)788776849','FaizMohammad','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Sediqa','Female Searcher ','0093(0)789250752','Sediqa','0000');
insert into `user`(name, position, phone, email, pin) VALUES('All Jhpiego Staff','Afghanistan Country Office','','Afghanistancountryoffice@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('IT Help Desk','AF IT Help Desk','','Afservice@jhpiego.org ','0000');
insert into `user`(name, position, phone, email, pin) VALUES('All Team Leaders','AF-HEMAYAT Team Leaders','','Hemayatteamleaders@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('All Technical Staff','AF-Hemayat-Technical-All','','AF-Hemayat-Technical@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('MER Department Staff','AF-MER-Jhpiego Staff','','AF-MER-Jhpiegostaff@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Procurement Bids/ Communication','AF-Procurement','','AF.procurement@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Call for applications/ proposals submissions for S D Department','AF-Hemayat-SDSubmissions','','Hemayat.Sdsubmissions@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Call for applications/ proposals submissions for Q I Department','AF-Hemayat-QISubmissions','','Hemayat.Qisubmissions@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Call for applications/ proposals submissions for MER Department','AF-Hemayat-MER','','HEMAYAT.MET@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Call for applications/ proposals submissions for Grants Department','AF-Grants Applications','','AF-GrantsApplications@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Innovations Proposals','AF Innovation Fund','','AF.InnovatoinFund@jhpiego.org','0000');
insert into `user`(name, position, phone, email, pin) VALUES('Call for applications/ proposals submissions for D G department','AF- Hemayat-DGSubmissions','','Hemayat.DGSubmissions@jhpiego.org','0000');

-- Deactivate out-bound users
update user set status = 'INACTIVE' where name like 'Benafsha%';
